# Use a Maven base image
FROM maven:3.8.4-openjdk-17 AS build

# Stage 1: Build the application
FROM maven:3.8.4-jdk-11-slim AS build

WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml .

# Resolve Maven dependencies (this step can be cached if the pom.xml doesn't change)
RUN mvn dependency:go-offline -B

# Copy the rest of the project source code to the container
COPY src ./src

# Build the project
RUN mvn compile dependency:copy-dependencies -DincludeScope=runtime

# Stage 2: Create the final Lambda image
FROM public.ecr.aws/lambda/java:11

# Set the working directory in the container
WORKDIR /var/task

# Copy the built JAR file and dependencies from the build stage
COPY --from=build /app/target/classes .
COPY --from=build /app/target/dependency/* ./lib/

# Define the Lambda handler
CMD ["org.trident.LambdaHandler::handleRequest"]

